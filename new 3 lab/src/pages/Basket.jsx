import List from "../components/List/List";
import { useOutletContext } from "react-router-dom";

function Basket() {
  const { basketProducts } = useOutletContext();
  let pageType = "presentIcon";
  return (
    <>
      <ul className="list">
        <List data={basketProducts} pageType={pageType} />
      </ul>
    </>
  );
}

export default Basket;
