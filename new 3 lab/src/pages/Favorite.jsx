import List from "../components/List/List";
import { useOutletContext } from "react-router-dom";

function Favorite() {
  const { favProducts } = useOutletContext();
  return (
    <>
      <ul className="list">
        <List data={favProducts} />
      </ul>
    </>
  );
}

export default Favorite;
