import { useState, useEffect } from "react";
import List from "../components/List/List";
import { Outlet } from "react-router-dom";

function MainPage() {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(`/products.json`);
      const info = await response.json();
      setData(info);
    };

    fetchData();
  }, []);
  return (
    <>
      <List data={data} />
    </>
  );
}

export default MainPage;
