import { Link } from "react-router-dom";
import { Outlet } from "react-router-dom";
import { useState, useEffect } from "react";
import basketIcon from "../components/assets/shopping-basket.svg";
import basicFavorite from "../components/assets/favorite-false.svg";
import "./Layout.scss";
import "./reset.scss";

function Layout() {
  const [basketProducts, setBasketProducts] = useState([]);
  const [favProducts, setFavProducts] = useState([]);

  const getDataFromLS = (key) => {
    const lsData = localStorage.getItem(key);
    if (!lsData) return [];
    try {
      const value = JSON.parse(lsData);
      return value;
    } catch (e) {
      return [];
    }
  };

  useEffect(() => {
    setBasketProducts(getDataFromLS("Basket"));
    setFavProducts(getDataFromLS("Favorite"));
  }, []);

  useEffect(() => {
    localStorage.setItem("Basket", JSON.stringify(basketProducts));
    localStorage.setItem("Favorite", JSON.stringify(favProducts));
  }, [basketProducts, favProducts]);
  return (
    <>
      <header className="header">
        <h1>Header Logo</h1>
        <div className="header__div">
          <img src={basketIcon} className="header__photo" alt="basket" />
          <p className="header__text">{basketProducts.length}</p>
          <img src={basicFavorite} className="header__photo" alt="favorite" />
          <p className="header__text">{favProducts.length}</p>
        </div>
      </header>
      <nav className="nav">
        <Link to="/" className="nav__link">
          Main Page
        </Link>
        <Link to="/basket" className="nav__link">
          Basket
        </Link>
        <Link to="/favorite" className="nav__link">
          Favorite
        </Link>
      </nav>

      <Outlet
        context={{
          setBasketProducts,
          setFavProducts,
          basketProducts,
          favProducts,
        }}
      />
    </>
  );
}

export default Layout;
