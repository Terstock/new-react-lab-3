import Card from "../Card/Card";
import "./List.scss";

function List({ data, pageType }) {
  return (
    <ul className="list">
      {data.map(({ vendor_code, ...item }) => (
        <Card
          card={{ ...item, vendor_code }}
          key={vendor_code}
          pageType={pageType}
        />
      ))}
    </ul>
  );
}

export default List;
