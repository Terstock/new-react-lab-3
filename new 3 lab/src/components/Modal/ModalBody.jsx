function ModalBody({ className, children }) {
  return <div className={className}>{children}</div>;
}

export default ModalBody;
